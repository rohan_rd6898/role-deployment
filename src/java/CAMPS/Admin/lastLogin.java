package CAMPS.Admin;

import CAMPS.Connect.connect;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class lastLogin extends HttpServlet {
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession ses = request.getSession(false);
        PrintWriter out = response.getWriter();
        try {
            connect con = new connect();
            con.conn();
            StringBuffer b = new StringBuffer();
            int i = 1;
            if (request.getParameter("id").equalsIgnoreCase("1")) {
                b.append("<table class=\"tbl\" width=\"500\" border=\"1\"><tr><th>IP Address</th><th> Login Time</th></tr>");
                con.read("SELECT ip,DATE_FORMAT(logintime,\"%d-%m-%Y %H:%i:%s\") logintime FROM admin.authentication_log al WHERE (username='" + ses.getAttribute("staffId") + "' or username='" + ses.getAttribute("email") + "') AND  STATUS=1 ORDER BY al.logintime DESC LIMIT 10");
                while (con.rs.next()) {
                    b.append("<tr><td>" + con.rs.getString("ip") + "</td><td>" + con.rs.getString("logintime") + "</td></tr>");
                }
                b.append("</table>");
            } else if (request.getParameter("id").equalsIgnoreCase("2")) {
                b.append("<table class=\"tbl\" width=\"500\" border=\"1\"><tr><th>Date</th><th> Session</th><th>Time</th></tr>");
                con.read("SELECT DATE_FORMAT(a.date,'%d-%m-%Y') AS `date`,a.sesssion,a.time FROM staff_time_exceed a WHERE a.staff_id='" + ses.getAttribute("userId") + "' AND DATE_FORMAT(a.date,'%Y-%m')=DATE_FORMAT(NOW(),'%Y-%m') AND a.time!='00:00:00'");
                while (con.rs.next()) {
                    b.append("<tr align='center'><td>" + con.rs.getString("date") + "</td><td>" + con.rs.getString("sesssion") + "</td><td>" + con.rs.getString("time") + "</td></tr>");
                }
                b.append("</table>");
            } else if (request.getParameter("id").equalsIgnoreCase("3")) {
                b.append("<table class=\"tbl\" width=\"500\" border=\"1\"><tr><th>S.No</th><th>IP</th><th>Date</th></tr>");
                con.read("SELECT a.ip,a.Logintime FROM admin.authentication_log a WHERE  DATE_FORMAT(a.Logintime,'%Y-%m-%d') IN( str_to_date('" + request.getParameter("date") + "','%d-%m-%Y')) and DATE_FORMAT(a.Logintime,'%H') BETWEEN '" + request.getParameter("time1") + "' AND '" + request.getParameter("time2") + "'  AND  (a.username='" + ses.getAttribute("staffId") + "' OR a.username='" + ses.getAttribute("email") + "') AND a.status=" + request.getParameter("status"));
                while (con.rs.next()) {
                    b.append("<tr align='center'><td>" + i + "</td><td>" + con.rs.getString("ip") + "</td><td>" + con.rs.getString("Logintime") + "</td></tr>");
                    i++;
                }
                b.append("</table>");
            } else if (request.getParameter("id").equalsIgnoreCase("4")) {  
                b.append("<table class=\"tbl\" width=\"500\" border=\"1\"><tr><th>S.No</th><th>Name</th><th>Roll No</th><th>Leave Type</th><th>Reason</th><th>From Date</th><th>From Time</th><th>To Date</th><th>To Time</th><th>Out Date</th><th>In Date</th></tr>");
                con.read("SELECT b.`student_name`,IFNULL(b.`roll_no`, b.enroll_no) AS roll_no,  c.`leave_type`,  a.`reason`,  DATE_FORMAT(a.from_date, '%d-%m-%Y') as from_date,  DATE_FORMAT(a.to_date, '%d-%m-%Y') as to_date,  DATE_FORMAT(    CONCAT('0000:00:00 ', from_time),    '%h:%i %p'  ) as from_time,  DATE_FORMAT(    CONCAT('0000:00:00 ', to_time),    '%h:%i %p'  ) as to_time,  ifnull(    DATE_FORMAT(a.`outdate`, '%d-%m-%Y %h:%i%p'),    ''  ) as outdate,  ifnull(    DATE_FORMAT(a.`indate`, '%d-%m-%Y %h:%i%p'),    ''  ) as indate,  a.app1,  a.app2,  a.app3 FROM  student_leave a   LEFT JOIN student_det_view b     ON a.`enroll_no` = b.`enroll_no`   LEFT JOIN student_leave_pattern c     ON a.`leave_type` = c.`id` WHERE a.id=" + request.getParameter("leave_id"));
                while (con.rs.next()) {
                    b.append("<tr align='center'><td>" + i + "</td><td>" + con.rs.getString("student_name") + "</td><td>" + con.rs.getString("roll_no") + "</td><td>" + con.rs.getString("leave_type") + "</td><td>" + con.rs.getString("reason") + "</td><td>" + con.rs.getString("from_date") + "</td><td>" + con.rs.getString("from_time") + "</td><td>" + con.rs.getString("to_date") + "</td><td>" + con.rs.getString("to_time") + "</td><td>" + con.rs.getString("outdate") + "</td><td>" + con.rs.getString("indate") + "</td></tr>");
                    i++;
                }
                b.append("</table>");      
            } 
            else if (request.getParameter("id").equalsIgnoreCase("5")) {   
                b.append("<table class=\"tbl\" width=\"500\" border=\"0\">");
                con.read("SELECT ic_query FROM integrity_check WHERE ic_id="+request.getParameter("ic_id")+"");
                while (con.rs.next()) {
                    b.append("<tr><td>" + con.rs.getString("ic_query") + "</td></tr>");
                   
                }
                b.append("</table>");
            } 
            else {
                 b.append("<table class=\"tbl\" width=\"500\" border=\"1\"><tr><th>S.No</th><th>Comp Date</th><th>Created Date</th><th>Remarks</th><th>Current Status</th><th>Expiry(in days)</th></tr>");
                con.read("SELECT a.comp_day,DATE_FORMAT(a.working_date,'%d-%m-%Y') comp_date,DATE_FORMAT(a.lastdate_modified,'%d-%m-%Y') ins_date,TIMESTAMPDIFF(DAY,DATE_FORMAT(lastdate_modified,'%Y-%m-%d'),NOW()),IF(a.status=1,IF((180-TIMESTAMPDIFF(DAY,DATE_FORMAT(lastdate_created,'%Y-%m-%d'),NOW()))>0,(180-TIMESTAMPDIFF(DAY,DATE_FORMAT(lastdate_modified,'%Y-%m-%d'),NOW())),0),0) AS count_com,IF(STATUS=0,'Availed',IF(TIMESTAMPDIFF(DAY,DATE_FORMAT(lastdate_modified,'%Y-%m-%d'),NOW()) <= 180,'Available','Elapsed')) comp_status FROM staff_compensationworkingday a WHERE staff_id ='"+request.getParameter("id")+"' AND a.status NOT IN (2) AND a.working_date>(NOW()-INTERVAL 12 MONTH) ORDER BY a.lastdate_modified");
                while (con.rs.next()) {
                    b.append("<tr><td>" + i + "</td><td>" + con.rs.getString("comp_date") + "</td><td>" + con.rs.getString("ins_date") + "</td><td>" + con.rs.getString("comp_day") + "</td><td>" + con.rs.getString("comp_status") + "</td><td>" + con.rs.getString("count_com") + "</td></tr>");  
                    i++;
                }
                b.append("</table>");
            }
            out.print(b);
            con.conclose();
        } catch (Exception e) {
            e.printStackTrace(out);
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
