package CAMPS.Admin;

import CAMPS.Connect.connect;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;

public class authenticationFilter implements Filter {
    private static final boolean debug = true;
    private ArrayList<String> urlList;
    private FilterConfig filterConfig = null;
    public PrintWriter a;

    public authenticationFilter() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            HttpSession session = ((HttpServletRequest) request).getSession();
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse res = (HttpServletResponse) response;
            a = res.getWriter();
            String url = req.getServletPath();
            SimpleDateFormat dtYMD = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date now = new Date();
            String hostname = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/";
            boolean allowedRequest = false;
            boolean reRirect = false;
            for (int i = 0; i < urlList.size(); i++) {
                if (url.contains(urlList.get(i))) {
                    allowedRequest = true;
                    break;
                }
            }

            if ( url.equals("/JSP/Welcome/reset_passwd.jsp")) {
            } else if (!allowedRequest) {
                connect obj = new connect();
                if (session.getAttribute("login") == null || session.getAttribute("login") == "false") {
                    res.sendRedirect(hostname + "index.jsp");
                    reRirect = true;
                }
                String roleid = null;
                if (session.getAttribute("roleId") != null) {
                    roleid = session.getAttribute("roleId").toString();
                }
                obj.conn();
                obj.read("SELECT menuid FROM admin2.role_resource_mapping WHERE roleid IN (" + roleid + ")");
                String[] menu = null;
                int incValue = 0;
                menu = new String[150];
                while (obj.rs.next()) {
                    menu[incValue] = obj.rs.getString("menuid");
                    incValue++;
                }
                ArrayList<String> menuidlist = new ArrayList<String>();
                for (int inc = 0; incValue > inc; inc++) {
                    String[] menu1 = menu[inc].split(",");
                    for (int inc1 = 0; menu1.length > inc1; inc1++) {
                        menuidlist.add(menu1[inc1]);
                    }
                }
                HashSet<String> hashSets = new HashSet<String>(menuidlist);
                menuidlist.clear();
                menuidlist = new ArrayList<String>(hashSets);
                Collections.sort(menuidlist);
                String menuid = StringUtils.join(menuidlist, ",");

                obj.read1("SELECT id,link FROM admin2.resource_master WHERE link = '" + url.substring(1) + "' AND Id IN(0," + menuid + ")");
                if (obj.rs1.first()) {
                    obj.insert("INSERT INTO admin2.authorization_log VALUES(page,'" + url.substring(1) + "',NOW(),'Authorized','" + session.getAttribute("userId").toString() + "','" + session.getAttribute("roleNames") + "');");
                } else {
                    obj.insert("INSERT INTO admin2.authorization_log VALUES(page,'" + url.substring(1) + "',NOW(),'Not  Authorized','" + session.getAttribute("userId").toString() + "','" + session.getAttribute("roleNames") + "');");
                    String a=req.getHeader("Referer");                        
                    if (req.getHeader("Referer") == null) {
                        res.sendRedirect(hostname + "JSP/Welcome/welcomePage.jsp?error_code=0");
                        reRirect = true;
                    }
                }
                obj.conclose();
            }
            Throwable problem = null;
            try {
                if (!reRirect) {
                    //chain.doFilter(new XSSRequestWrapper((HttpServletRequest) request), response);
                    chain.doFilter(request, response);
                }
            } catch (Throwable t) {
                problem = t;
                t.printStackTrace();
            }
            if (problem != null) {
                if (problem instanceof ServletException) {
                    throw (ServletException) problem;
                }
                if (problem instanceof IOException) {
                    throw (IOException) problem;
                }
            }
        } catch (Exception ex) {

            ex.printStackTrace(a);
        }
    }

    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    public void destroy() {
    }

    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        String urls = filterConfig.getInitParameter("avoid-urls");
        StringTokenizer token = new StringTokenizer(urls, ",");

        urlList = new ArrayList<String>();

        while (token.hasMoreTokens()) {
            String a = token.nextToken();
            urlList.add(a);
        }
        if (filterConfig != null) {
            if (debug) {
            }
        }
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }
}
