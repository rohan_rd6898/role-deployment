package CAMPS.Admin;

import CAMPS.Common.escapeSpecialChars;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import CAMPS.Connect.connect;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;

public class checkLogin extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession ses = request.getSession(true);
        PrintWriter out = response.getWriter();
        connect con = new connect();            
        try {
            request.getServletContext().setAttribute("hostname", (Object) (request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/"));
            con.conn();
            escapeSpecialChars objesc = new escapeSpecialChars();
             ResourceBundle rb = ResourceBundle.getBundle("CAMPS.Admin.Psalt");
            String uname = null;
            boolean Admin = false;
            String ip = request.getHeader("X-FORWARDED-FOR");
            request.getHeader("VIA");
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }
            ip = ipAddress;
            String status = "";
            uname = objesc.escapeSpecialChar(request.getParameter("uname"));
            String gkey = (String) ses.getAttribute("gkey");
            if (gkey != null) {
                con.read("select *,DATE_FORMAT(LastLoginTime,\"%d-%m-%Y %H:%i:%s\") AS lastlogin from admin2.user_master where userid=(select staff_id from staff_personal_view where (current_email='" + gkey + "' or off_email='" + gkey + "')) ");
            } else {
               //con.read("SELECT *,DATE_FORMAT(LastLoginTime,\"%d-%m-%Y %H:%i:%s\") AS lastlogin FROM admin2.user_master WHERE (upassword=(SELECT SHA1(SHA1(MD5(CONCAT('"+rb.getString("psalt.first")+"',salt.pwdSalt,'" + objesc.escapeSpecialChar(request.getParameter("pword")) + "',salt.pwdSalt,'"+rb.getString("psalt.last")+"')))) AS pwd  FROM admin2.user_master salt WHERE username='admin' AND `status`= 1) OR upassword=(SELECT SHA1(SHA1(MD5(CONCAT('"+rb.getString("nsalt.first")+"',salt.pwdSalt,'" + objesc.escapeSpecialChar(request.getParameter("pword")) + "',salt.pwdSalt,'"+rb.getString("nsalt.last")+"')))) AS pwd  FROM admin2.user_master salt WHERE username='admin' AND `status`= 1))");
                //if (con.rs.first()) {
                    Admin = true;
                //}
                
                if (Admin) {
                    con.read("SELECT *,DATE_FORMAT(LastLoginTime,\"%d-%m-%Y %H:%i:%s\") AS lastlogin FROM admin2.user_master WHERE username='" + uname + "' ");
                    //Admin = true;
                } else {
                    con.read("SELECT *,DATE_FORMAT(LastLoginTime,\"%d-%m-%Y %H:%i:%s\") AS lastlogin FROM admin2.user_master WHERE (upassword=(SELECT SHA1(SHA1(MD5(CONCAT('"+rb.getString("psalt.first")+"',salt.pwdSalt,'" + objesc.escapeSpecialChar(request.getParameter("pword")) + "',salt.pwdSalt,'"+rb.getString("psalt.last")+"')))) AS pwd  FROM admin2.user_master salt WHERE username='"+uname+"' AND `status`>0) OR upassword=(SELECT SHA1(SHA1(MD5(CONCAT('"+rb.getString("nsalt.first")+"',salt.pwdSalt,'" + objesc.escapeSpecialChar(request.getParameter("pword")) + "',salt.pwdSalt,'"+rb.getString("nsalt.last")+"')))) AS pwd  FROM admin2.user_master salt WHERE username='"+uname+"' AND `status`>0))");
                }
            }
            if (con.rs.first()) {
                if (con.rs.getRow() == 1) {
                    status = "1";
                    ses.setAttribute("login", con.rs.getString("userId"));
                    ses.setAttribute("roleId", con.rs.getString("roleId")); 
                    ses.setAttribute("userId", con.rs.getString("userId"));
                    ses.setAttribute("lastlogintime", con.rs.getString("lastlogin"));
                    con.read1("SELECT CONCAT(legend,'.',staff_name) AS NAME,CONCAT(dep_idCode,staff_id) AS staffid,cur_emailid,dep_id,STATUS FROM staff_personal_copy,department_master WHERE dep_id=dept_id AND staff_id='" + con.rs.getString("userId") + "'");
                    if (con.rs1.next()) {
                        ses.setAttribute("userName", con.rs1.getString("name"));
                        ses.setAttribute("staffId", con.rs1.getString("staffid"));
                        ses.setAttribute("depId", con.rs1.getString("dep_id"));
                        ses.setAttribute("email", con.rs1.getString("cur_emailid"));

                        if (con.rs1.getInt("status") > 0) {
                            ses.setAttribute("uType", "Staff");
                        } else {
                            ses.setAttribute("uType", "Student");
                        }
                    }
                    con.read1("SELECT StartUpPage,roleName FROM admin2.role_master WHERE id IN (" + con.rs.getString("roleId") + ") ORDER BY startuppage DESC");
                    ArrayList<String> rolename = new ArrayList<String>();
                    while (con.rs1.next()) {
                        rolename.add(con.rs1.getString("roleName"));
                    }
                    con.read2("SELECT * FROM lib_users lu WHERE lu.user_id='" + con.rs.getString("userId") + "' GROUP BY lu.user_id");
                    if (!con.rs2.next()) {
                        ses.setAttribute("libId", "1");
                    }
                    String rolename1 = StringUtils.join(rolename, (String) ",");
                    this.log("dfgsdfg= " + rolename1);
                    ses.setAttribute("roleNames", rolename1);
                    if (con.rs1.first() && con.rs.getInt("status") ==1) {
                        String startpage = con.rs1.getString("StartUpPage");
                        this.log(startpage);
                        this.log("UPDATE admin2.user_master SET LastLoginTime=NOW() WHERE username='" + con.rs.getString("username") + "'");
                        if (!Admin) {
                            con.update("UPDATE admin2.user_master SET LastLoginTime=NOW() WHERE username='" + con.rs.getString("username") + "'");
                        }
                        if (startpage != null && !startpage.isEmpty()) {
                            response.sendRedirect(startpage);
                        } else {
                            response.sendRedirect("JSP/Welcome/welcomePage.jsp");
                        }
                    } else if (con.rs.getInt("status")==2) {
                        response.sendRedirect("JSP/Welcome/reset_passwd.jsp");
                    }else {
                        response.sendRedirect("JSP/Welcome/welcomePage.jsp");
                    }
                }
            } else { 
                int flag = 0;
                if (gkey != null || Admin) {
                    con.read("SELECT sad.id,sad.student_name,IFNULL(sad.roll_no,sad.enroll_no) roll_no,sad.enroll_no,sad.dept_id,sad.student_official_email_id,IFNULL(DATE_FORMAT(a.Logintime,\"%d-%m-%Y %H:%i:%s\"),'')Logintime,sad.student_status FROM student_det_view sad LEFT OUTER JOIN admin2.authentication_log a ON a.username=sad.student_official_email_id WHERE ((sad.student_official_email_id='" + gkey + "' or sad.roll_no='" + uname + "' or sad.parent_email_id='" + gkey + "' ) AND sad.student_status LIKE 'cont%') OR  ((sad.student_official_email_id='" + gkey + "' or sad.roll_no='" + request.getParameter("uname") + "' or sad.student_email_id='" + gkey + "' ) AND sad.student_status LIKE 'comple%')    ORDER BY a.Logintime DESC LIMIT 1");
                    if (con.rs.next()) {
                        try {
                            ses.setAttribute("userName", con.rs.getString("student_name"));
                            ses.setAttribute("staffId", con.rs.getString("roll_no"));
                            ses.setAttribute("login", con.rs.getString("enroll_no"));
                            if(con.rs.getString("student_status").equalsIgnoreCase("CONTINUING")){
                            ses.setAttribute("roleId", "48");
                            ses.setAttribute("roleNames", "Student(Portal)");
                            }else{
                            ses.setAttribute("roleId", "42");
                            ses.setAttribute("roleNames", "Alumni(Portal)"); 
                            }
                            ses.setAttribute("lastlogintime", con.rs.getString("Logintime"));
                            ses.setAttribute("depId", con.rs.getString("dept_id"));
                            ses.setAttribute("email", con.rs.getString("student_official_email_id"));
                            ses.setAttribute("userId", con.rs.getString("enroll_no"));
                            ses.setAttribute("uType", "Student"); 
                            ses.setAttribute("libId", "1");
                            con.read1("SELECT rm.startUpPage FROM admin2.role_master rm WHERE rm.id IN ("+ses.getAttribute("roleId")+") and rm.startUpPage is not null UNION SELECT \"JSP/Welcome/welcomePage.jsp\"  LIMIT 1 ");
                            if(con.rs1.next()){
                            response.sendRedirect(con.rs1.getString("startUpPage"));
                            }
                            flag = 1;
                            status = "1";
                        } catch (Exception e) {
                            out.print(e);
                        }
                    }
                }
                if (flag == 0) {
                    if (gkey != null) {
                        con.read("select `status` from admin2.user_master where userid=(select staff_id from staff_personal_view where current_email='" + gkey + "') ");
                    } else {
                        con.read("SELECT `status` FROM admin2.user_master WHERE username='" + uname + "'");
                    }
                    if (con.rs.first() && con.rs.getInt("status") == 0) {
                        status = "2";
                        response.sendRedirect(this.getServletContext().getAttribute("hostname").toString() + "CommonJSP/login.jsp?er_c=1");
                    }
                    status = "0";
                    response.sendRedirect(this.getServletContext().getAttribute("hostname").toString() + "CommonJSP/login.jsp?er_c=0");
                }
            }
            if (gkey != null) {
                con.insert("INSERT INTO admin2.authentication_log VALUES('" + ip + "',NOW(),'" + gkey + "','" + status + "');");
            } else if (!Admin) {
                con.insert("INSERT INTO admin2.authentication_log VALUES('" + ip + "',NOW(),'" + uname + "','" + status + "');");
            } else if (Admin) {
                con.insert("INSERT INTO admin2.authentication_log VALUES('" + ip + "',NOW(),'" + uname + "','99');");
            } else {
                con.insert("INSERT INTO admin2.authentication_log VALUES('" + ip + "',NOW(),'" + uname + "','98');");
            }
            //MAC_Detection mac=new  MAC_Detection(ip);
            //mac.run();
            
        } catch (SQLException e) {
           //response.sendRedirect(this.getServletContext().getAttribute("hostname").toString() + "CommonJSP/login.jsp?er_c=0");
           out.print(e);
        } catch (Exception e) {
            //response.sendRedirect(this.getServletContext().getAttribute("hostname").toString() + "CommonJSP/login.jsp?er_c=0");
        } finally {
            try {
                out.close();
                con.conclose();
            } catch (SQLException ex) {
                Logger.getLogger(checkLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    public String getServletInfo() {
        return "Short description";
    }
}
