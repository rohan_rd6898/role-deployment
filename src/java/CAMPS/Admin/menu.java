package CAMPS.Admin;

import CAMPS.Connect.connect;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;

public class menu {

    connect obja = new connect();
    String menu1 = "";

    public String displaymenu(int parent, String role, String hostname) {
        try {
            String link = "", menuid;
            ArrayList<String> menuidArr = new ArrayList<String>();
            menuidArr.clear();
            obja.conn();
            obja.read1("SELECT menuid FROM admin2.role_resource_mapping WHERE roleid IN (" + role + ")");
            while (obja.rs1.next()) {
                menuidArr.add(obja.rs1.getString("menuid"));
            }
            menuid = StringUtils.join(menuidArr, ",");
            obja.read("SELECT a.id, a.label, a.link, Deriv1.Count,IFNULL(a.extraParameters,'') AS extraParameters FROM admin2.resource_master a  "
                    + "LEFT OUTER JOIN (SELECT parentId, COUNT(*) AS COUNT FROM admin2.resource_master "
                    + "GROUP BY parentId) Deriv1 ON a.id = Deriv1.parentId WHERE a.parentId=" + parent
                    + " AND a.id IN(" + menuid + ") ORDER BY a.sortOrder ASC");
            if (parent == 0) {
                menu1 += "<ul id=\"css3menu1\" class=\"topmenu\">";
            } else {
                menu1 += "<ul>";
            }
            while (obja.rs.next()) {
                if (obja.rs.getString("extraParameters").equalsIgnoreCase("2")) {
                    continue;
                }
                if (obja.rs.getString("link").equalsIgnoreCase("#")) {
                    link = "#";
                } else if (obja.rs.getString("extraParameters").equalsIgnoreCase("1")) {
                    link = obja.rs.getString("link");
                } else if (obja.rs.getString("extraParameters").equalsIgnoreCase("")) {
                    link = hostname + obja.rs.getString("link");
                }
                if (obja.rs.getInt("Count") > 0) {
                    menu1 += "<li><a href='" + link + "' title='" + obja.rs.getString("label") + "' >" + obja.rs.getString("label") + " </a>";
                    menu a = new menu();
                    menu1 += a.displaymenu(obja.rs.getInt("id"), role, hostname);
                    menu1 += "</li>";
                } else if (obja.rs.getInt("Count") == 0) {

                    if (parent == 0) {
                        if (obja.rs.getString("extraParameters").equalsIgnoreCase("1")) {
                            menu1 += "<li><a href='" + link + "' title='" + obja.rs.getString("label") + "' target='_blank'>" + obja.rs.getString("label") + "</a></li>";
                        } else {
                            menu1 += "<li><a href='" + link + "' title='" + obja.rs.getString("label") + "' >" + obja.rs.getString("label") + "</a></li>";
                        }
                    } else if (obja.rs.getString("extraParameters").equalsIgnoreCase("1")) {
                        menu1 += "<li><a href='" + link + " ' title='" + obja.rs.getString("label") + "' target='_blank'>" + obja.rs.getString("label") + "</a></li>";
                    } else {
                        menu1 += "<li><a href='" + link + " ' title='" + obja.rs.getString("label") + "' >" + obja.rs.getString("label") + "</a></li>";
                    }
                }
            }

            if (parent == 0) {
                menu1 += "";
            } else {
                menu1 += "</ul>";
            }
            obja.conclose();
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return menu1;
    }
}
