
<%@page import="CAMPS.Connect.connect"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <link href="../css/loginStyle.css" rel="stylesheet" type="text/css"></link>
        <title>CAMPS LOGIN</title>
        <meta name="description" content="Faculty/Student can Login Faculty/Student can Login Faculty/Student can Login Faculty/Student can Login Faculty/Student can Login " />
        <meta name="keywords" content="camps, Bit camps, bitsathy camps, camps bitsathy, camps bit" />
        <meta name="robots" content="noindex,nofollow"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1" />
        <noscript>
        <meta http-equiv="refresh" content="0; url=../static/Scriptcheck.html"></meta>
        </noscript>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <link REL="SHORTCUT ICON" href="../Images/favicon.ico"></link>
        <link href="../css/redmond/jquery-ui-1.8.16.custom.css" type="text/css" rel="stylesheet"></link><!--css --> 
        <link href="../css/validationEngine.jquery.css" type="text/css" rel="stylesheet"></link>
        <script language="javascript" type="text/javascript" src="../scripts/jquery-1.7.1.js"></script>
        <script language="javascript" type="text/javascript" src="../scripts/jquery-ui-1.8.16.custom.min.js"></script>
        <script language="javascript" type="text/javascript" src="../scripts/jquery.validationEngine.js"></script>
        <script language="javascript" type="text/javascript" src="../scripts/jquery.validationEngine-en.js"></script>
        <link rel="stylesheet" href="../css/slider/bjqs.css" />
        <script src="../scripts/slider/bjqs-1.3.min.js"></script>
        <link type="text/css" rel="stylesheet" href="../css/notify/ui.notify.css" />
        <script src="../scripts/notify/jquery.notify.js" type="text/javascript"></script>
        <style>
            #or {  position: absolute; top: 125px; width: 40px; height: 16px;line-height: 16px;color: #666;font-weight: bold;text-align: center;font-size: 12px;  }
            #loginHead {position: absolute; top: 510px; left: 480px;width: 500px;color: #FFFFFF;text-align: center;font-size: 18px;}
            input#uname{ background-image:url(../Images/user_ico.png); background-size: 10% ;background-position:100%;}
            input#pword{ background-image:url(../Images/pass_ico.png);background-size: 10% ; background-position:100%;}
            input#captcha_ans{ background-image:url(../Images/images.jpg);background-size: 10% ; background-position:100%;}
            A:hover {font-size:30; font-weight:bold; color: white;}
        </style>
        <script language="javascript">

            $("#form").validationEngine();
            $("#uname").focus();


        </script>
    </head>
    <body onload="disableBackButton();" onpageshow="if (event.persisted) disableBackButton();" style="height:100%;">

        <script>
            $(function () {
                $("#dialog").dialog({autoOpen: false, resizable: false, width: 350, height: 450, });
                $("#dialog").dialog({position: {my: "right center", at: "right center", of: window}});
                $("#dialog").dialog("open");
                $("#button").on("click", function () {
                    $("#dialog").dialog("open");
                });
            });
        </script>
        <script class="secret-source">
            jQuery(document).ready(function ($) {
                history.pushState(null, null, document.URL);
                window.addEventListener('popstate', function (event) {
                    history.pushState(null, null, document.URL);
                });
                $('#banner-fade').bjqs({animtype: 'fade', height: 430, width: 350, responsive: true, showcontrols: false, centercontrols: false, showmarkers: false});
                $("#form").validationEngine();
                $("#uname").focus();
                var count = 0;
                $container = $("#container").notify();
                create("sticky", {title: 'Default Notification', text: 'Example of a default notification.  I will fade out after 5 seconds'}, {expires: 40000,
                    click: function (e, instance) {
                        if (count == 0)
                            create("sticky1", {title: 'Default Notification', text: 'Example of a default notification.  I will fade out after 5 seconds'}, {expires: 30000});
                        count = 1;
                    }
                });

            });

        </script>

        <div id="header"></div>
        <%
            application.setAttribute("hostname", request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/");
            int error_code = -1;
            if (request.getParameter("er_c") != null) {
                error_code = Integer.parseInt(request.getParameter("er_c").toString());
            }
        %>

        <div style="height:100%;min-height:100%; height:auto !important;margin: -50px auto -80px;">
            <table width="100%" border="0" style="height:100%;position:absolute;"  >
                <tr>
                    <td align="center" height="100%">
                        <form id="form" action="../checkLogin" method="post">
                            <table  border="0" align="center" class="" >
                                <tr>
                                    <td  align="center"><img src="../Images/camps-logo1.png" alt="" width="137" height="50"/></td>
                                </tr>
                                <tr>    
                                    <td><input type="text" name="uname" id="uname" class="validate[required,custom[onlyLetterNumber]] text-input"  placeholder="username"/></td>
                                </tr>
                                <tr> <td><input type="password" name="pword" id="pword" class="validate[required,custom[varchar]]" placeholder="password"/></td></tr>
                              <tr> <td align="right"><button type="submit" name="login" id="login" value="Login" >Login </button> </td>
                                </tr>
                                <tr>
                                    <td align="center" width="100px" style="color: #F5F5F5">or</td>
                                </tr>
                                <tr><td align="center">
                                        <a href='#' style='text-decoration: none' onClick="var win = window.open('#');
                                                setTimeout(function () {
                                                    win.close();
                                                    window.location.href = '#';
                                                }, 1200);
                                           ">
                                            <img  src="../Images/gsignin.png" alt="Sign in with Google" title="Bitsathy mail(for Student/Faculty),Gmail(for Parent) account login" width="100%" height="40" /></a>        
                                    </td>
                                </tr> 
                                <tr><td><a href="../student/Password_mail.jsp" style="font-size: 13px" >Forgot password?</a></td></tr>
                            </table> </form>

                        <% if (error_code == 0) {%>
                        <div class="error">
                            <img src="../Images/cross.png" />
                            Incorrect username / Password
                        </div>
                        <%
                        } else if (error_code == 1) {
                        %>
                        <div class="warning">
                            <img src="../Images/alert.png" />
                            The account is deactivated contact Administrator
                        </div>
                        <% }%>
                    </td>
                </tr >
            </table>
        </div> 
        <div class="footer" id="footer" >
            <table width="100%"><tbody><tr><td style="vertical-align: middle;" align="right"><div style="margin-top: 15px;"><span style="color: rgba(0, 0, 0, 0.26);font-style: italic;"><b>Powered by </b></span><img src="../Images/ssl/sdc-Logo.png" style="height: 50px;"></div></td></tr></tbody></table>
            <!-- end .footer --></div>
    </body>
</html>
