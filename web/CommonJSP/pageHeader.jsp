
<%@page import="CAMPS.Connect.connect"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="CAMPS.Admin.menu"%>


<html>
    <head>
        <script type="text/javascript">
        </script>
    <noscript>
        <meta http-equiv="refresh" content="0; url=../../static/Scriptcheck.html">       
    </noscript>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link REL="SHORTCUT ICON" href="../../Images/favicon.ico">
    <link href="../../css/redmond/jquery-ui-1.8.16.custom.css" type="text/css" rel="stylesheet"><!--css -->
    <link rel="stylesheet" type="text/css" href="../../css/menu.css">
    <link rel="stylesheet" type="text/css" href="../../css/DefaultCss.css">
    <link href="../../css/validationEngine.jquery.css" type="text/css" rel="stylesheet">
    <link href="../../css/table.css" rel="stylesheet" type="text/css">
    <link href="../../css/colorbox.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../../css/chosen/chosen.css" />

    <script language="javascript" type="text/javascript" src="../../scripts/jquery-1.7.1.js"></script>
    <script language="javascript" type="text/javascript" src="../../scripts/jquery-ui-1.8.16.custom.min.js"></script>
    <script language="javascript" type="text/javascript" src="../../scripts/jquery.validationEngine.js"></script>
    <script language="javascript" type="text/javascript" src="../../scripts/jquery.validationEngine-en.js"></script>
    <script language="javascript" type="text/javascript" src="../../scripts/autocomplete.js"></script>
    <script language="javascript" type="text/javascript" src="../../scripts/jquery.colorbox.js"></script>
    <script language="javascript" type="text/javascript" src="../../scripts/form_scripts.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.multiselect.js"></script>
    <script type="text/javascript" src="../../scripts/jquery.multiselect.filter.js"></script>
    <script src="../../scripts/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="../../scripts/jquery.tablesorter.min.js" type="text/javascript"></script>
    <script src="../../scripts/jquery.tablesorter.pager.js" type="text/javascript"></script>
    <style>
        .nav-counter { position: absolute;top: 100px;right: 1px;min-width: 8px;height: 20px;line-height: 20px; margin-top: -11px; padding: 0 6px;font-weight: normal;color: white;text-align: center;position:absolute;            padding-right:2px;padding-left:2px;background-color:red;color:white;font-weight:bold;font-size:0.80em;border-radius:2px;box-shadow:1px 1px 1px gray;font-family:Tahoma;font-size:10px;        }
        .nav-counter-green { background: #75a940; border: 1px solid #42582b;background-image: -webkit-linear-gradient(top, #8ec15b, #689739); background-image: -moz-linear-gradient(top, #8ec15b, #689739);            background-image: -o-linear-gradient(top, #8ec15b, #689739);            background-image: linear-gradient(to bottom, #8ec15b, #689739);        }
    </style>

    <script language="javascript" type="text/javascript">
            var windowwidth = 0;
            $(document).ready(function () {
                windowwidth = $(window).width();
                history.pushState(null, null, document.URL);
                window.addEventListener('popstate', function (event) {
                    history.pushState(null, null, document.URL);
                });
                $("#cssmenu").menumaker({
                    format: "multitoggle"
                });
                $("#scrolltop").click(function () {
                    $("html, body").animate({scrollTop: 0}, "slow");
                    return false;
                });
                $("#scrollbottom").click(function () {
                    $("html, body").animate({scrollTop: $(document).height()}, "slow");
                    return false;
                });
                if ($(document).height() > 1200)
                    $("#scrolltop,#scrollbottom").show();
                else
                    $("#scrolltop,#scrollbottom").hide();

                $('.tbl').find('tr:odd').each(function (index, element) {
                    $(this).addClass('off');
                });
                $('.tbl').find('tr:even').each(function (index, element) {
                    $(this).addClass('on');
                });
            });
            var currentTime = "<%=new java.text.SimpleDateFormat("dd-MM-yy").format(new java.util.Date())%>";
            function disableBackButton()
            {
                window.history.forward();
            }
            setTimeout("disableBackButton()", 0);
            function changepwd(link)
            {
                $().colorbox({
                    opacity: .8, transition: "",
                    onCleanup: function () {
                        $('#changepassword').validationEngine('hideAll');
                    },
                    onClosed: function () {
                        $("#changepassword").validationEngine('detach');
                    },
                    speed: 400, width: "500", href: link});
            }
    </script>
</head>
<body onLoad="disableBackButton();" onpageshow="if (event.persisted) disableBackButton();">  
    <div class="nav_up" id="scrolltop">Scroll to Top</div>
    <div class="nav_down" id="scrollbottom">Scroll to Bottom</div>
    <div id="header" align="center">
        <table align="center">
            <tr>
                <td align="center" >
                    <%
                        String hostname = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
                        String roleid = "";
                        try {
                            if (session.getAttribute("roleId") != null) {
                                roleid = session.getAttribute("roleId").toString();
                            } else {
                                response.sendRedirect("../CommonJSP/login.jsp");
                            }
                        } catch (Exception e) {
                            out.print(e);
                        }
                        menu menuobj = new menu();
                        out.println("<header><nav id='cssmenu'><div id='head-mobile'></div><div class='button'></div>" + menuobj.displaymenu(0, roleid, hostname));
                    %>

            <li><a href='<%=hostname + "JSP/Welcome/welcomePage.jsp"%>' title='Home' ><img src="../../Images/home.png"  height="24" style="margin:-5px;"/></a></li>
            <li><a href='http://www.bitsathy.ac.in' target='_blank' title='Go to BITSATHY'><img src="../../Images/bit.png"  height="24" style="margin:-5px;"/></a></li>
            <li><a title='Setting' ><img src="../../Images/setting.png" style="margin:-5px;" height="24"/></a>
                <ul>
                    <% try {
                            if (session.getAttribute("uType").toString().equalsIgnoreCase("Staff")) {
                    %> <li><a title="Change password" href="#" onClick="changepwd('<%=hostname + "CommonJSP/changePassword.jsp"%>')">Change password</a> </li><% }
                        } catch (Exception e) {
                        }%>
                    <li>
                        <a title="Log out" href="<%=hostname + "logout"%>">Log out</a>
                    </li>
                </ul></li></nav></header>
            <script>
                $(function () {
                    var button = $('#settingsbutton');
                    var menu = $('.menulist');

                    $('ul li a', menu).each(function () {
                        $(this).append('<span />');
                    });
                    button.toggle(function (e) {
                        e.preventDefault();
                        menu.css({display: 'block'});
                        $('.ar', this).html('&#9650;').css({top: '3px'});
                        $(this).addClass('active');
                    }, function () {
                        menu.css({display: 'none'});
                        $('.ar', this).html('&#9660;').css({top: '5px'});
                        $(this).removeClass('active');
                    });
                });
                (function ($) {
                    $.fn.menumaker = function (options) {
                        var cssmenu = $(this), settings = $.extend({
                            format: "multitoggle",
                            sticky: false
                        }, options);
                        return this.each(function () {
                            $(this).find(".button").on('click', function () {

                                $(this).toggleClass('menu-opened');
                                var mainmenu = $(this).next('ul');
                                if (mainmenu.hasClass('open')) {
                                    mainmenu.slideToggle().removeClass('open');
                                } else {
                                    mainmenu.slideToggle().addClass('open');
                                    if (settings.format === "dropdown") {
                                        mainmenu.find('ul').show();
                                    }
                                }
                            });
                            cssmenu.find('li ul').parent().addClass('has-sub');
                            multiTg = function () {
                                cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                                cssmenu.find('.submenu-button').on('click', function () {
                                    $(this).toggleClass('submenu-opened');
                                    if ($(this).siblings('ul').hasClass('open')) {
                                        $(this).siblings('ul').removeClass('open').slideToggle();
                                    } else {
                                        $(this).siblings('ul').addClass('open').slideToggle();
                                    }
                                });
                            };
                            if (settings.format === 'multitoggle')
                                multiTg();
                            else
                                cssmenu.addClass('dropdown');
                            if (settings.sticky === true)
                                cssmenu.css('position', 'fixed');
                            resizeFix = function () {
                                var mediasize = 700;
                                if ($(window).width() > mediasize) {
                                    cssmenu.find('ul').show();
                                }
                                if ($(window).width() <= mediasize && windowwidth !== $(window).width()) {
                                    cssmenu.find('ul').hide().removeClass('open');
                                    windowwidth = $(window).width()
                                }
                            };
                            resizeFix();
                            return $(window).on('resize', resizeFix);
                        });
                    };
                })(jQuery);

            </script>
            </td>
            </tr>
            <tr class="welcome">
                <td align="left"> <div style="float:left;width:60%;">Welcome <b><span class="headername"><% try {
                        out.print(session.getAttribute("userName").toString() + " (" + session.getAttribute("staffId").toString() + ") </span></b></div> <div style=\"float:right;width:40%;text-align:right;\">Last Login: " + session.getAttribute("lastlogintime").toString() + "");
                    } catch (Exception e) {
                    }%> <a href="#" onclick="last_login(1)">Details</a></div></td>
                                </tr>
                                </table></div>
                                <div style="height:100%;min-height:100%; height:auto !important;margin: -75px auto -100px;" align='center'>
                                    <div style="padding-top:75px;"></div>
