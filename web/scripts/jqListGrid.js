/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function($)
{
    var dataList = new Array();
    var userBeforeRequest = undefined;
    var listParamName = undefined;
    var count = 0;
    var methods = 
    {
        init: function(options,listPName)
        {
            return this.each(function()
            {
                if(typeof(options)=='undefined')
                {
                    $.error('jqGridList: Initialization options is undefined!');
                    return;
                }
                dataList = new Array();
                userBeforeRequest = undefined;
                listParamName = undefined;
                count = 0;
                options.ajaxGridOptions = {
                    async:false
                };
                listParamName = typeof(listPName)=='string' ? listPName : 'dataList';
                userBeforeRequest = options.beforeRequest;
                options.beforeRequest = listRequestCustomizer;
                $(this).jqGrid(options);
            });
        },
        add: function(data)
        {
            return this.each(function()
            {
                data = data.toUpperCase();
                if(typeof(data)=='undefined')
                    return;
                else if(typeof(data)=='string')
                {
                    if(dataList.indexOf(data)==-1)
                    {
                        dataList.push(data);
                        $(this).trigger('reloadGrid');
                        nCount = $(this).jqGrid('getGridParam', 'records');
                        if(nCount==count+1)
                            count = nCount;
                        else
                            dataList.pop();
                    }
                }
                else
                {
                    $.error('jqGridList: attempt to add a non-string!');
                }
            });
        },
        removeLast: function()
        {
            return this.each(function()
            {
                dataList.pop();
                $(this).trigger('reloadGrid');
                count = dataList.length;
            });
        },
        removeAll: function()
        {
            return this.each(function()
            {
                dataList.splice(0,dataList.length)
                $(this).trigger('reloadGrid');
                count = dataList.length;
            });
        },
        remove: function(data)
        {
            return this.each(function()
            {
                if(typeof(data)=='undefined')
                    return;
                else if(typeof(data)=='string')
                {
                    var index = dataList.indexOf(data);
                    if(index != -1)
                        dataList.splice(index,1);
                }
                else
                {
                    $.each(data,function(id,val)
                    {
                        var index = dataList.indexOf(val);
                        if(index != -1)
                            dataList.splice(index,1);
                    });
                }
                $(this).trigger('reloadGrid');
                count = dataList.length;
            });
        },
        removeSelected: function()
        {
            selectedItems = $(this).jqGrid('getGridParam','selarrrow');
            
            $.each(selectedItems,function(id,val)
            {
                var index = dataList.indexOf(val);
                if(index != -1)
                    dataList.splice(index,1);
            });
            $(this).trigger('reloadGrid');
            count = dataList.length;
        },
        getLast: function()
        {
            return dataList[dataList.length-1];
        },
        getAsCSV: function()
        {
            return dataList.join(",");
        },
        getAsQuotedCSV: function()
        {
            var csvList = dataList.join("','");
            if(csvList!='') csvList = '\''+csvList+'\'';
            return csvList;
        }
    };
    var listRequestCustomizer = function()
    {
        $(this).jqGrid('clearGridData');

        var csvList = dataList.join("','");
        if(csvList!='')
            csvList = '\''+csvList+'\'';
        else
            csvList='\'IMPOSSIBLE VALUE\'';
            
        postParams = $(this).getGridParam('postData');
        postParams[listParamName] = csvList;
        $(this).setGridParam('postData',postParams);

        if(typeof(userBeforeRequest)=='function')
        {
            return userBeforeRequest.apply(this);
        }
        else
        {
            return true;
        }
    }
    $.fn.jqListGrid = function(method)
    {
        if(methods[method])
        {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        }
        else if(typeof method === 'object' || ! method)
        {
            return methods.init.apply( this, arguments );
        }
        else
        {
            $.error( 'Method ' +  method + ' does not exist on jQuery.jqListGrid' );
        }
    }
})(jQuery);
