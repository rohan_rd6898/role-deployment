/*include the following lines in your jsp page to convert the enter key to tab.
$(document).ready(function(){
    $("input,select,textarea").enter2tab(); 

   });
*/
jQuery.fn.enter2tab = function(option)
{
    var settings={
                    unhideIfHidden:false,
                    unhideParentsIfHidden:false,
                    returnIfNoNext:false,
                    hiddenReturn:false
                };
   $.extend(settings,option);
  this.keypress(function(e)
  {
    if($(this).attr('type')=='submit' || $(this).attr('type')=='reset')
    {
        return true;
    }
    var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
    var tmp = null;
    var maxTabIndex = 250;
    var nTabIndex=this.tabIndex+1;
    var myNode=this.nodeName.toLowerCase();
    if(nTabIndex > 0 && key == 13 && nTabIndex <= maxTabIndex && ((myNode == "textarea") || (myNode == "input") || (myNode == "select") ))
    {
        for (var x=0; x<3; x++)
        {
            tmp = $("a[tabIndex='"+nTabIndex+"'],textarea[tabIndex='"+nTabIndex+"'],select[tabIndex='"+nTabIndex+"'],input[tabIndex='"+nTabIndex+"']").get(0);
            if (typeof tmp != "undefined" && !$(tmp).attr("disabled"))
            {
                if($(tmp).is(':hidden'))
                {
                    if(settings.unhideParentsIfHidden)
                    {
                        $(tmp).show();
                        if($(tmp).is(':hidden'))
                        {
                            $(tmp).parents().each(function(id)
                            {
                                $(this).show();
                            });
                        }
                        $(tmp).focus();
                        return false;
                    }
                    else if(settings.unhideIfHidden)
                    {
                        $(tmp).show();
                        if($(tmp).is(':hidden'))
                        {
                            return settings.hiddenReturn;
                        }
                        $(tmp).focus();
                        return false;
                    }
                    else
                    {
                        return settings.hiddenReturn;
                    }
                    return false;
                }
                $(tmp).focus();
                return false;
            }
            else
            {
                nTabIndex++;
            }
        }
        return settings.returnIfNoNext;
    }
    else if(key == 13)
    {
        return false;
    }
  });
  return this;
}
     